module gitlab.com/msvechla/vaultbot

require (
	github.com/alecthomas/assert v1.0.0
	github.com/alecthomas/assert/v2 v2.0.2
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/aws/aws-sdk-go v1.37.30
	github.com/fatih/color v1.10.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.8 // indirect
	github.com/hashicorp/vault/api v1.3.0
	github.com/hashicorp/vault/api/auth/gcp v0.1.0
	github.com/hashicorp/vault/sdk v0.3.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/kami-zh/go-capturer v0.0.0-20171211120116-e492ea43421d
	github.com/pavel-v-chernykh/keystore-go v2.1.0+incompatible
	github.com/pierrec/lz4 v2.6.0+incompatible // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gotest.tools v2.2.0+incompatible // indirect
	gotest.tools/v3 v3.1.0
	software.sslmate.com/src/go-pkcs12 v0.2.0
)

go 1.16
